void SwitchRelayOn_Private(short switchNb)
{
  if(RO_FLUSH_SOLENOID_RELAY == switchNb)
     digitalWrite(switchNb,HIGH);
  else 
     digitalWrite(switchNb,LOW);
}


void SwitchRelayOff_Private(short switchNb)
{
  if(RO_FLUSH_SOLENOID_RELAY == switchNb)
     digitalWrite(switchNb,LOW);
  else 
     digitalWrite(switchNb,HIGH);
}


#define InvalidRelayPin  100
static short relays[NbRelays] = {InvalidRelayPin,InvalidRelayPin,InvalidRelayPin,InvalidRelayPin,InvalidRelayPin,InvalidRelayPin,InvalidRelayPin};
static bool status[NbRelays] = {false,false,false,false,false,false,false};

void ChangeRelayStatus(short relayNb, bool trueForOn)
{
  for(short i = 0; i<NbRelays ; i++)
  {
    if(relays[i] == InvalidRelayPin)
    {
      if(trueForOn)
        SwitchRelayOn_Private(relayNb);
      else
        SwitchRelayOff_Private(relayNb);

      relays[i] = relayNb;
      status[i] = trueForOn;
      break;
    }
    if(relays[i] == relayNb)
    {
      if(status[i] != trueForOn)
      {
         if(trueForOn)
           SwitchRelayOn_Private(relayNb);
         else
            SwitchRelayOff_Private(relayNb);
         status[i] = trueForOn;        
      }
    }
  }
}
