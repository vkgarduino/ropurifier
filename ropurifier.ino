#include "InitialSetup.h"



void StopROMachine() 
{
  ChangeRelayStatus(UV_AND_PUMP_RELAY,false);
  ChangeRelayStatus(CLEANWATER_OUTPUT_WASHING_RELAY,false);
  ChangeRelayStatus(CLEANWATER_OUTPUT_AQUARIUM_RELAY,false);
  ChangeRelayStatus(CLEANWATER_OUTPUT_RO_TANK_RELAY,false);
  ChangeRelayStatus(AFTER_PUMP_NORMAL_WATER_SOLENOID_RELAY,false);
  ChangeRelayStatus(AFTER_PUMP_RO_WATER_SOLENOID_RELAY,false);
  ChangeRelayStatus(RO_FLUSH_SOLENOID_RELAY,false);
}


bool FlushRoIfNecessary()
{
    static const long PerformFlushInHowManyMilliSeconds = 3600000; 
    static unsigned long timeInMillis_LastFlush = 0;
    unsigned long timeInMillis = millis();
    static bool firstTimeFlushCompleted = false;
    if( !firstTimeFlushCompleted || timeInMillis < timeInMillis_LastFlush || (timeInMillis - timeInMillis_LastFlush) > PerformFlushInHowManyMilliSeconds) 
    {
      firstTimeFlushCompleted = true;
      ChangeRelayStatus(CLEANWATER_OUTPUT_WASHING_RELAY,false);
      ChangeRelayStatus(CLEANWATER_OUTPUT_AQUARIUM_RELAY,false);
      ChangeRelayStatus(CLEANWATER_OUTPUT_RO_TANK_RELAY,false);
      ChangeRelayStatus(UV_AND_PUMP_RELAY,true);
      ChangeRelayStatus(AFTER_PUMP_NORMAL_WATER_SOLENOID_RELAY,false);
      ChangeRelayStatus(AFTER_PUMP_RO_WATER_SOLENOID_RELAY,true);
      ChangeRelayStatus(RO_FLUSH_SOLENOID_RELAY,true);
      LockRoScreenWithProgressBar("Ro Flushing..",7000);
      StopROMachine();
      timeInMillis_LastFlush = millis();
      return true;
  }
  return false;
}

static unsigned long timeInMillis_LastRest = 0;
bool RestRoIfNecessary()
{
  static const unsigned long PerformRestInHowManyMilliSeconds = 900000; 
  unsigned long timeInMillis = millis();
  if( timeInMillis < timeInMillis_LastRest || (timeInMillis - timeInMillis_LastRest) > PerformRestInHowManyMilliSeconds) 
  {
    StopROMachine();
    LockRoScreenWithProgressBar("Ro resting..",300000);
    timeInMillis_LastRest = millis();
    return true;
  }
  return false;
}

void SwitchOnCorrectDispensingRelay(Machine_Status machineStatus) 
{
  ChangeRelayStatus(UV_AND_PUMP_RELAY,true);
  if(machineStatus == E_RoTankWaterOutput)
  {
    ChangeRelayStatus(CLEANWATER_OUTPUT_WASHING_RELAY,false);
    ChangeRelayStatus(CLEANWATER_OUTPUT_AQUARIUM_RELAY,false);
    ChangeRelayStatus(CLEANWATER_OUTPUT_RO_TANK_RELAY,true);//ro water
  }
  else if(machineStatus == E_WashineWaterOutput)
  {
    ChangeRelayStatus(CLEANWATER_OUTPUT_WASHING_RELAY,true);//washing water
    ChangeRelayStatus(CLEANWATER_OUTPUT_AQUARIUM_RELAY,false);
    ChangeRelayStatus(CLEANWATER_OUTPUT_RO_TANK_RELAY,false);
  }
  else if(machineStatus == E_AquariumWaterOutput)
  {
    ChangeRelayStatus(CLEANWATER_OUTPUT_WASHING_RELAY,false);
    ChangeRelayStatus(CLEANWATER_OUTPUT_AQUARIUM_RELAY,true);//aquarium water
    ChangeRelayStatus(CLEANWATER_OUTPUT_RO_TANK_RELAY,false);
  }
}

static unsigned long timeInMillis_SinceDispensing = 0;
void DispenseWaterAction(Machine_Status machineStatus, WATER_QUALITY quality) 
{
  SwitchOnCorrectDispensingRelay(machineStatus);
  unsigned long timeInMillis = millis();
  if( timeInMillis < timeInMillis_SinceDispensing)
    timeInMillis_SinceDispensing = timeInMillis;
  unsigned long timeInMillis_SinceDispensing = timeInMillis_SinceDispensing - timeInMillis;

  if(quality == LEVEL1)
  {
    ChangeRelayStatus(AFTER_PUMP_RO_WATER_SOLENOID_RELAY,true);
    ChangeRelayStatus(AFTER_PUMP_NORMAL_WATER_SOLENOID_RELAY,false);
  }
  else if(quality == LEVEL5)
  {
    ChangeRelayStatus(AFTER_PUMP_RO_WATER_SOLENOID_RELAY,false);
    ChangeRelayStatus(AFTER_PUMP_NORMAL_WATER_SOLENOID_RELAY,true);
  }
  else
  {
    ChangeRelayStatus(AFTER_PUMP_RO_WATER_SOLENOID_RELAY,true);
    unsigned long inManyMillisecondsStartNormalWaterInOneMinute = 2000;//2 seconds
    if(quality == LEVEL2)
      inManyMillisecondsStartNormalWaterInOneMinute = 1000;
    else if(quality == LEVEL3)
      inManyMillisecondsStartNormalWaterInOneMinute = 2000;
    else if(quality == LEVEL4)
      inManyMillisecondsStartNormalWaterInOneMinute = 3000;
    
    unsigned long temp = 60000 - inManyMillisecondsStartNormalWaterInOneMinute;
    if(timeInMillis_SinceDispensing%60000 >= temp)
       ChangeRelayStatus(AFTER_PUMP_NORMAL_WATER_SOLENOID_RELAY,true);
    else
       ChangeRelayStatus(AFTER_PUMP_NORMAL_WATER_SOLENOID_RELAY,false);
  }
}


Machine_Status GetMachineStatus()
{
  Machine_Status machineStatus;
  if(!IsSwitchOn(LOW_PRESSURE_SWITCH))
    machineStatus = E_NotEnoughWaterPressure;
  else if(IsSwitchOn(WASHING_WATER_SWITCH))
    machineStatus = E_WashineWaterOutput;
  else if(IsSwitchOn(ROTANK_WATER_SWITCH))
    machineStatus = E_RoTankWaterOutput;
  else if(IsSwitchOn(AQUARIUM_WATER_SWITCH))
    machineStatus = E_AquariumWaterOutput;
  else
    machineStatus = E_RoTankIsFull; 
  return machineStatus;
}

WATER_QUALITY GetWaterQuality(Machine_Status machineStatus)
{
  WATER_QUALITY quality = WATER_OUTPUT_DEVICES_QUALITY[0];
  if(machineStatus == E_RoTankWaterOutput)
     quality = WATER_OUTPUT_DEVICES_QUALITY[0];
  else if(machineStatus == E_WashineWaterOutput)
     quality = WATER_OUTPUT_DEVICES_QUALITY[1];
  else if(machineStatus == E_AquariumWaterOutput)
     quality = WATER_OUTPUT_DEVICES_QUALITY[2];
  return quality;
}

void ReadOrSaveSetting()
{
  static bool settingRead = false;
  if(settingRead == false)
  {
    settingRead = true;
    ReadSettings();//read setting only once
  }
  SaveSettings();//save setting only once
}

void loop() 
{
  delay(100);
  ReadOrSaveSetting();
  Machine_Status machineStatus = GetMachineStatus();

  //***************************************perform machine actions***************************************
  if(machineStatus == E_NotEnoughWaterPressure || machineStatus == E_RoTankIsFull)
  {
    timeInMillis_LastRest = millis();//as machine is stopped it is equivalent to ro taking rest
    timeInMillis_SinceDispensing = millis();
    StopROMachine();
  }
  else 
  {
    WATER_QUALITY quality = GetWaterQuality(machineStatus);
    bool val1 = RestRoIfNecessary();
    bool val2 = FlushRoIfNecessary();
    if(val1 || val2)//previous quality was poor then flush uv
      timeInMillis_SinceDispensing = millis();
    DispenseWaterAction(machineStatus,quality);
  }

  //***************************************display setting menu***************************************
  bool isSettingMenuShown = false;
  if(machineStatus != E_NotEnoughWaterPressure)
    isSettingMenuShown = DisplaySettingMenu(machineStatus);
  
  //***************************************display menu (except setting)***************************************
  DisplayTimelyMenu(isSettingMenuShown, machineStatus);

}
