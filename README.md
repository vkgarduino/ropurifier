# RoPurifier

This is arduino project, which i created to control my Ro purifier (Aquaguard xpert), once its microprocess gone kaput.

There are 3 type of output water dispension:

1. Ro Water
2. Washing water
3. Aquarium Water.

For either of 3 categories there are following settings:
a. Soft - Pure RO water
b. Mix1
c. Mix2
d. Mix3
e. Hard - Ro filter is off

