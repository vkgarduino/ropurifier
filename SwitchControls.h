
bool IsSwitchOn(short switchNb)
{
   if(switchNb == A6 || switchNb == A7)
   {
     int val = analogRead(switchNb);
     if(val > 700)
         return false;
     return true;
   }
   else
   {
      int sensorVal = digitalRead(switchNb);
      if (sensorVal == HIGH) 
      {
         return false;
      } 
      else 
      {
         return true;
      }
   } 
}
