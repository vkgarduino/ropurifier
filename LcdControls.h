#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 16, 2);



void SetLcdCursor(byte column, byte row)
{
   lcd.setCursor(column,row);
}
void InitLcdRow(int row)
{
   lcd.setCursor(0,row);
   lcd.print("                ");
   SetLcdCursor(0,row);
}

void EmptyLcd()
{
  InitLcdRow(0);
  InitLcdRow(1);
  SetLcdCursor(0,0);
}

void AppendTextInLcd(const char * iString)
{
   lcd.print(iString);
}


void LcdSleep()
{
   lcd.noBacklight();
}

void LcdGetUp()
{
   lcd.backlight();
}

void InitializeLcd()
{
  lcd.begin();
  LcdGetUp();
  lcd.noBlink();
}

void ShowContentInLcd(const char* firstLineConent1, const char* firstLineConent2, const char* firstLineConent3, const char* firstLineConent4,
                      const char* secondLineConent1, const char* secondLineConent2, const char* secondLineConent3, const char* secondLineConent4)
{
    EmptyLcd();
    if(firstLineConent1)
       AppendTextInLcd(firstLineConent1);
    if(firstLineConent2)
       AppendTextInLcd(firstLineConent2);
    if(firstLineConent3)
       AppendTextInLcd(firstLineConent3);
    if(firstLineConent4)
       AppendTextInLcd(firstLineConent4);
    InitLcdRow(1);
    if(secondLineConent1)
       AppendTextInLcd(secondLineConent1);
    if(secondLineConent2)
       AppendTextInLcd(secondLineConent2);
    if(secondLineConent3)
       AppendTextInLcd(secondLineConent3);
    if(secondLineConent4)
       AppendTextInLcd(secondLineConent4);
    LcdGetUp();
}


void DisplayTimelyMenu(bool isSettingMenuShown, Machine_Status iMachineStatus)
{
  unsigned long timeInMillis = millis();
  static unsigned long timeInMillis_SinceDisplayingStarted = 0;
  if(timeInMillis < timeInMillis_SinceDisplayingStarted)
    timeInMillis_SinceDisplayingStarted = timeInMillis;
  if(isSettingMenuShown)
  {
     timeInMillis_SinceDisplayingStarted = timeInMillis;
     return;
  }
  
  unsigned long howManyMilliSecondToDisplay = 4000;
  unsigned long howManyMilliSecondToWait = 2000;
  if( E_RoTankIsFull == iMachineStatus)
    howManyMilliSecondToWait = 20000;//wait for 20 seconds after each display

  unsigned long timeInMillis_ElapsedSinceLastDisplay = timeInMillis - timeInMillis_SinceDisplayingStarted;
  unsigned long tempTime = timeInMillis_ElapsedSinceLastDisplay % (howManyMilliSecondToDisplay + howManyMilliSecondToWait);
  static bool displayShown = false;
  if(tempTime < howManyMilliSecondToDisplay && !displayShown)
  {
    //Serial.println("display on");
    displayShown = true;
    if(E_NotEnoughWaterPressure == iMachineStatus)
      ShowContentInLcd("Low Water Press",0,0,0,"Pressure",0,0,0);
    else if(E_RoTankWaterOutput == iMachineStatus)
      ShowContentInLcd("RO Tank Filling",0,0,0,"      (",WATER_QUALITY_NAMES[WATER_OUTPUT_DEVICES_QUALITY[0]],")",0);
    else if(E_WashineWaterOutput == iMachineStatus)
      ShowContentInLcd("Washing Water",0,0,0,"      (",WATER_QUALITY_NAMES[WATER_OUTPUT_DEVICES_QUALITY[1]],")",0);
    else if(E_AquariumWaterOutput == iMachineStatus)
      ShowContentInLcd("Aquarium Water",0,0,0,"      (",WATER_QUALITY_NAMES[WATER_OUTPUT_DEVICES_QUALITY[2]],")",0);
    else if(E_RoTankIsFull == iMachineStatus)
      ShowContentInLcd("RO Water Full",0,0,0,"   (",WATER_QUALITY_NAMES[WATER_OUTPUT_DEVICES_QUALITY[0]],")",0);
  }
  else if(tempTime > howManyMilliSecondToDisplay && displayShown)
  {
    //Serial.println("display off");
    displayShown = false;
    EmptyLcd();
    LcdSleep();
  }
}


bool DisplaySettingMenu(Machine_Status iMachineStatus)
{
  //returns if setting menu is shown
  static unsigned long timeInMillis_LastButtonPress = 0;
  unsigned long timeInMillis = millis();
  if(timeInMillis < timeInMillis_LastButtonPress)
    timeInMillis_LastButtonPress = 0;
    
  static bool isSettingMenuShown = false;
  if( (timeInMillis - timeInMillis_LastButtonPress) > 5000)
  {
    if(isSettingMenuShown)
    {
         EmptyLcd();
         LcdSleep();
    }
    isSettingMenuShown = false;
  }
  bool left_Chessis_Switch = IsSwitchOn(CHESSIS_LEFT_SWITCH);
  bool right_Chessis_Switch = IsSwitchOn(CHESSIS_RIGHT_SWITCH);
  if(left_Chessis_Switch || right_Chessis_Switch)
  {
    Buzzer_ChesisButtonPressed();
    timeInMillis_LastButtonPress = timeInMillis;
    enum SettingPage{SettingPageOne=0,SettingPageTwo};
    enum SettingPageOneOptions{RoWater_Setting=0,WashingWater_Setting,AquariumWater_Setting,E_InvalidSettingPageOneOptions};
    const char * SettingPageOneNames[E_InvalidSettingPageOneOptions] = {"RO Tank","Washing","Aquarium"};


    static SettingPage currentSettingPage = SettingPageOne; 
    static SettingPageOneOptions selectedOptionInFirstSettingPage = RoWater_Setting;
    static WATER_QUALITY selectedQualityInSecondSettingPage = LEVEL1;
    
    /*currentSettingMenuPage = SettingPageOne  ->  row 1: "which output"
     *                            row 2: "washing, aquarium, ro   => fills curentOutputInSettingMenu
     *currentSettingMenuPage = SettingPageTwo  ->  row 1: "quality level"
     *                            row 2: "ro water, level 1, ... , Normal" => fills WASHING_WATER_QUALITY, AQUARIUM_WATER_QUALITY, RO_WATER_QUALITY*/
    if(isSettingMenuShown == false)
    {
       isSettingMenuShown = true;
       currentSettingPage = SettingPageOne;
       selectedOptionInFirstSettingPage = RoWater_Setting;  //initialize this setting 
    }
    else
    {
       if(left_Chessis_Switch)
       {
          if(currentSettingPage == SettingPageOne)
          {
            selectedOptionInFirstSettingPage = (SettingPageOneOptions)((int)selectedOptionInFirstSettingPage + 1);
            if(selectedOptionInFirstSettingPage == E_InvalidSettingPageOneOptions)
               selectedOptionInFirstSettingPage = (SettingPageOneOptions) 0;
          }
          else if(currentSettingPage == SettingPageTwo)
          {
               WATER_QUALITY newQuality = (WATER_QUALITY)((int)selectedQualityInSecondSettingPage + 1);
               if (newQuality == E_InvalidWaterQualityOption)
                  newQuality = (WATER_QUALITY) 0;
               selectedQualityInSecondSettingPage = newQuality;
          }
       }
       else if(right_Chessis_Switch)
       {
          if(currentSettingPage == SettingPageOne)
          {
            currentSettingPage = SettingPageTwo;
            if( selectedOptionInFirstSettingPage == RoWater_Setting)
               selectedQualityInSecondSettingPage = WATER_OUTPUT_DEVICES_QUALITY[0];
            else if( selectedOptionInFirstSettingPage == WashingWater_Setting)
               selectedQualityInSecondSettingPage = WATER_OUTPUT_DEVICES_QUALITY[1];
            else if( selectedOptionInFirstSettingPage == AquariumWater_Setting)
               selectedQualityInSecondSettingPage = WATER_OUTPUT_DEVICES_QUALITY[2];
          }
          else if(currentSettingPage == SettingPageTwo)
          {
               if( selectedOptionInFirstSettingPage == RoWater_Setting)
                  WATER_OUTPUT_DEVICES_QUALITY[0] = selectedQualityInSecondSettingPage;
               else if( selectedOptionInFirstSettingPage == WashingWater_Setting)
                  WATER_OUTPUT_DEVICES_QUALITY[1] = selectedQualityInSecondSettingPage;
               else if( selectedOptionInFirstSettingPage == AquariumWater_Setting)
                  WATER_OUTPUT_DEVICES_QUALITY[2] = selectedQualityInSecondSettingPage;
               isSettingMenuShown = false;
          }
       }
    }

    if( SettingPageOne == currentSettingPage)
       ShowContentInLcd("Select Setting..",0,0,0,"  ",SettingPageOneNames[selectedOptionInFirstSettingPage],0,0);
    else
       ShowContentInLcd("Select Quality..",0,0,0,"  ",WATER_QUALITY_NAMES[selectedQualityInSecondSettingPage],0,0);
  }
  return isSettingMenuShown;
}

void LockRoScreenWithProgressBar(const char * message, unsigned long timeInMilliseconds)
{
    Buzzer_RoOrUVFlush();
    EmptyLcd();
    AppendTextInLcd(message);
    InitLcdRow(1);
    LcdGetUp();
    unsigned long progress = 15;
    unsigned long progessStep = timeInMilliseconds/progress;
    for(short i = 0; i< progress; i++)
    {
      AppendTextInLcd("*");
      delay(progessStep);
    }
    EmptyLcd();
    LcdSleep(); 
}
