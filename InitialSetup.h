
#define BUZZER_PIN 2
#define WASHING_WATER_SWITCH 3
#define AQUARIUM_WATER_SWITCH 4
#define ROTANK_WATER_SWITCH 5
#define LOW_PRESSURE_SWITCH 6
#define CHESSIS_LEFT_SWITCH 8
#define CHESSIS_RIGHT_SWITCH 7
#define CLEANWATER_OUTPUT_WASHING_RELAY 9
#define CLEANWATER_OUTPUT_AQUARIUM_RELAY 10
#define CLEANWATER_OUTPUT_RO_TANK_RELAY 11
#define RO_FLUSH_SOLENOID_RELAY 12 //behaves reverse
#define AFTER_PUMP_NORMAL_WATER_SOLENOID_RELAY A0
#define AFTER_PUMP_RO_WATER_SOLENOID_RELAY A1
#define UV_AND_PUMP_RELAY A2
#define NbRelays 7


enum WATER_QUALITY{LEVEL1=0,LEVEL2=1,LEVEL3=2,LEVEL4=3,LEVEL5=4,E_InvalidWaterQualityOption};
const char *WATER_QUALITY_NAMES[E_InvalidWaterQualityOption] = {"Soft", "Mix1", "Mix2", "Mix3", "Hard" };

const short NB_OUTPUT_DEVICES=3;
WATER_QUALITY WATER_OUTPUT_DEVICES_QUALITY[NB_OUTPUT_DEVICES] = {LEVEL1,LEVEL2,LEVEL5};//Ro Quality, Washing Quality, Aquarium Quality

enum Machine_Status{E_NotEnoughWaterPressure=0,E_RoTankWaterOutput, E_WashineWaterOutput,E_AquariumWaterOutput, E_RoTankIsFull};

#include "BuzzerControls.h"
#include "SwitchControls.h"
#include "RelayControls.h"
#include "SavingControls.h"
#include "LcdControls.h"

void InitializeButton(short pin)
{
  if(pin == A6 || pin == A7)
  {
     pinMode(pin, INPUT);
  }
  else
  {
    pinMode(pin, INPUT_PULLUP);
  }
}

void setup(){
  pinMode(BUZZER_PIN, OUTPUT); 
  InitializeButton(CHESSIS_LEFT_SWITCH);
  InitializeButton(CHESSIS_RIGHT_SWITCH);
  InitializeButton(WASHING_WATER_SWITCH);
  InitializeButton(AQUARIUM_WATER_SWITCH);
  InitializeButton(ROTANK_WATER_SWITCH);
  InitializeButton(LOW_PRESSURE_SWITCH);

  pinMode(CLEANWATER_OUTPUT_WASHING_RELAY,OUTPUT);
  pinMode(CLEANWATER_OUTPUT_AQUARIUM_RELAY,OUTPUT);
  pinMode(CLEANWATER_OUTPUT_RO_TANK_RELAY,OUTPUT);
  pinMode(RO_FLUSH_SOLENOID_RELAY,OUTPUT);
  pinMode(AFTER_PUMP_NORMAL_WATER_SOLENOID_RELAY,OUTPUT);
  pinMode(AFTER_PUMP_RO_WATER_SOLENOID_RELAY,OUTPUT);
  pinMode(UV_AND_PUMP_RELAY,OUTPUT);

  ChangeRelayStatus(CLEANWATER_OUTPUT_WASHING_RELAY,false);
  ChangeRelayStatus(CLEANWATER_OUTPUT_AQUARIUM_RELAY,false);
  ChangeRelayStatus(CLEANWATER_OUTPUT_RO_TANK_RELAY,false);
  ChangeRelayStatus(RO_FLUSH_SOLENOID_RELAY,false);
  ChangeRelayStatus(AFTER_PUMP_NORMAL_WATER_SOLENOID_RELAY,false);
  ChangeRelayStatus(AFTER_PUMP_RO_WATER_SOLENOID_RELAY,false);
  ChangeRelayStatus(UV_AND_PUMP_RELAY,false);

  InitializeLcd();
  EmptyLcd();
  LcdSleep();
  Serial.begin(9600);

  ShowContentInLcd("Hello!",0,0,0,0,0,0,0);
  delay(1000);
}
