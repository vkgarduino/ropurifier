
#include <EEPROM.h>


int currentAddress = 0;
void StartReadingOrSaving()
{
  currentAddress = 0;
}



void UpdateValueFromCurrentAddressAndIncrementCurrentAddress(byte value)
{
  if( currentAddress < EEPROM.length())
  {
    EEPROM.update(currentAddress,value);
    currentAddress++;
  }
}


void ReadSerializedValue(byte &value)
{
  //if( currentAddress < EEPROM.length())
  {
    value = EEPROM.read(currentAddress);
    currentAddress++;
  }
}
const byte SerializationMechanismValidityDigit = 200;
const byte SerializationMechanismValidityDigit2 = 201;
const byte SerializationMechanismValidityDigit3 = 201;

void WriteValidityDigit(byte validityDigitType = 1)
{
  byte value = SerializationMechanismValidityDigit;
  if(validityDigitType == 2)
    value = SerializationMechanismValidityDigit2;
  if(validityDigitType == 3)
    value = SerializationMechanismValidityDigit3;
   
  UpdateValueFromCurrentAddressAndIncrementCurrentAddress(value);
}


bool CheckValidityDigit(byte validityDigitType = 1)
{
  byte value = SerializationMechanismValidityDigit;
  if(validityDigitType == 2)
    value = SerializationMechanismValidityDigit2;
  if(validityDigitType == 3)
    value = SerializationMechanismValidityDigit3;
   
  byte validity = 0;
  ReadSerializedValue(validity);
  if(validity != value)
     return false;
  return true;
}

void SaveSettings()
{
  StartReadingOrSaving();

  //started
  WriteValidityDigit(1);

  for(short i = 0; i<NB_OUTPUT_DEVICES; i++)
  {
    WriteValidityDigit(2);
    UpdateValueFromCurrentAddressAndIncrementCurrentAddress((byte)WATER_OUTPUT_DEVICES_QUALITY[i]);
  }
  
  WriteValidityDigit(3);
}


bool ReadSettings()
{
  StartReadingOrSaving();
  if(!CheckValidityDigit(1))
    return false;


  for(short i = 0; i<NB_OUTPUT_DEVICES; i++)
  {
    if(!CheckValidityDigit(2))
      return false;
    byte val = 0;
    ReadSerializedValue(val);
    WATER_OUTPUT_DEVICES_QUALITY[i] = (WATER_QUALITY) val;
    if(WATER_OUTPUT_DEVICES_QUALITY[i] < 0 || WATER_OUTPUT_DEVICES_QUALITY[i] > LEVEL5)
    {
       WATER_OUTPUT_DEVICES_QUALITY[i] = LEVEL1;
    }
    //Serial.println(i);
    //Serial.println(WATER_OUTPUT_DEVICES_QUALITY[i]);
  }
  
  if(!CheckValidityDigit(3))
    return false;
    
  return true;
}
